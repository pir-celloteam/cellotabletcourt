package net.pirsquare.cello.court.utils;

/**
 * Created by kampolleelaporn on 3/29/16.
 */
public class Constant {

    /**
     * Release
     */
    public static final String SECRET_KEY = "cello:pirsquare.net";
    public static final String METEOR_SERVER_URL = "ws://192.168.2.49:4343/websocket";
    public static final String API_SERVER_URL = "http://api.q-happy.com/tablets/v1.1/";
    public static final String API_QR_URL = "https://d1saoc02k62vdj.cloudfront.net/qr/";

    public static final int FIRST_FLOOR = 7;
    public static final int SECOND_FLOOR = 4;
    public static final int THIRD_FLOOR = 2;

    public static final int START_FIRST_FLOOR = 0;
    public static final int START_SECOND_FLOOR = 7;
    public static final int START_THIRD_FLOOR = 11;
}