package net.pirsquare.cello.court.model;

import java.util.List;

/**
 * Created by kung on 12/19/16.
 */

public class BrandData {

    /**
     * branch : {"_id":"LWlkA4GC54","name":"ถนนสรรพาวุธ","index":56,"brandId":"ZwKL8ANIxW","services":[{"id":"1GVr29PI3K","icon_url":null,"is_allow_capacity":"no","name":"ฝากขัง-หมายค้น-หมายจับ","capacity_unit":"คน"},{"id":"1GVr2P6hEm","icon_url":null,"is_allow_capacity":"no","name":"งานประกันตัว","capacity_unit":"คน"},{"id":"44po5qLup9","icon_url":null,"is_allow_capacity":"no","name":"รับฟ้องอายา","capacity_unit":"คน"},{"id":"9bx8pxeh8a","icon_url":null,"is_allow_capacity":"no","name":"การเงินและบัญชี","capacity_unit":null},{"id":"DWaVnOlcBD","icon_url":null,"is_allow_capacity":"no","name":"งานไกล่เกลี่ย","capacity_unit":"คน"},{"id":"ja6Og46hBo","icon_url":null,"is_allow_capacity":"no","name":"งานสารบรรณ","capacity_unit":"คน"},{"id":"nLAW8rXtkq","icon_url":null,"is_allow_capacity":"no","name":"อุธรณ์-ฎีกา","capacity_unit":"คน"},{"id":"pQqE14ju9Z","icon_url":null,"is_allow_capacity":"no","name":"งานบังคับคดี","capacity_unit":"คน"},{"id":"QjnP484f4b","icon_url":null,"is_allow_capacity":"no","name":"ศูนย์หน้าบัลลังก์","capacity_unit":"คน"},{"id":"qOP8d4JtzG","icon_url":null,"is_allow_capacity":"no","name":"ศูนย์นัดความ","capacity_unit":"คน"},{"id":"rQeyAzJSlR","icon_url":null,"is_allow_capacity":"no","name":"เก็บสำนวนแดง","capacity_unit":"คน"},{"id":"wdlamrJSay","icon_url":null,"is_allow_capacity":"no","name":"ศูนย์คัดถ่าย","capacity_unit":"คน"},{"id":"YwkL4raIoV","icon_url":null,"is_allow_capacity":"no","name":"เก็บสำนวนดำ","capacity_unit":"คน"},{"id":"zajm3K9fJz","icon_url":null,"is_allow_capacity":"no","name":"รับฟ้องแพ่ง","capacity_unit":"คน"}]}
     * configs : {"clientType":"local_server","allowGroupManage":true,"logo":null,"printers":{"logoTop":null,"logoBottom":null},"defaultSettings":{"allowReserve":true,"groupManagement":[{"serviceId":"9bx8pxeh8a","maxCapacity":1,"minCapacity":1,"allowBooking":false},{"serviceId":"zajm3K9fJz","minCapacity":1,"maxCapacity":1,"allowBooking":false},{"serviceId":"44po5qLup9","minCapacity":1,"maxCapacity":1,"allowBooking":false},{"serviceId":"YwkL4raIoV","minCapacity":1,"maxCapacity":1,"allowBooking":false},{"serviceId":"rQeyAzJSlR","minCapacity":1,"maxCapacity":1,"allowBooking":false},{"serviceId":"nLAW8rXtkq","minCapacity":1,"maxCapacity":1,"allowBooking":false},{"serviceId":"1GVr29PI3K","minCapacity":1,"maxCapacity":1,"allowBooking":false},{"serviceId":"wdlamrJSay","minCapacity":1,"maxCapacity":1,"allowBooking":false},{"serviceId":"QjnP484f4b","minCapacity":1,"maxCapacity":1,"allowBooking":false},{"serviceId":"qOP8d4JtzG","minCapacity":1,"maxCapacity":1,"allowBooking":false},{"serviceId":"1GVr2P6hEm","minCapacity":1,"maxCapacity":1,"allowBooking":false},{"serviceId":"pQqE14ju9Z","minCapacity":1,"maxCapacity":1,"allowBooking":false},{"serviceId":"ja6Og46hBo","minCapacity":1,"maxCapacity":1,"allowBooking":false},{"serviceId":"DWaVnOlcBD","minCapacity":1,"maxCapacity":1,"allowBooking":false}]},"awsS3":{"S3_BUCKET_NAME":"biz.cello.db","S3_KEY_DIRECTORY":"data/"},"isHoldable":true,"holdExpireSecond":60}
     * deviceSettings : {"client":{"osVersion":"10.0.14393","deviceType":"Windows_NT","appVersion":"","hostName":"DESKTOP-UO4BHH1"},"connections":{"tv":"","printer":""}}
     * createdAt : {"$date":1482146491351}
     */

    private BranchBean branch;
    private ConfigsBean configs;
    private DeviceSettingsBean deviceSettings;
    private CreatedAtBean createdAt;

    public BranchBean getBranch() {
        return branch;
    }

    public void setBranch(BranchBean branch) {
        this.branch = branch;
    }

    public ConfigsBean getConfigs() {
        return configs;
    }

    public void setConfigs(ConfigsBean configs) {
        this.configs = configs;
    }

    public DeviceSettingsBean getDeviceSettings() {
        return deviceSettings;
    }

    public void setDeviceSettings(DeviceSettingsBean deviceSettings) {
        this.deviceSettings = deviceSettings;
    }

    public CreatedAtBean getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(CreatedAtBean createdAt) {
        this.createdAt = createdAt;
    }

    public static class BranchBean {
        /**
         * _id : LWlkA4GC54
         * name : ถนนสรรพาวุธ
         * index : 56
         * brandId : ZwKL8ANIxW
         * services : [{"id":"1GVr29PI3K","icon_url":null,"is_allow_capacity":"no","name":"ฝากขัง-หมายค้น-หมายจับ","capacity_unit":"คน"},{"id":"1GVr2P6hEm","icon_url":null,"is_allow_capacity":"no","name":"งานประกันตัว","capacity_unit":"คน"},{"id":"44po5qLup9","icon_url":null,"is_allow_capacity":"no","name":"รับฟ้องอายา","capacity_unit":"คน"},{"id":"9bx8pxeh8a","icon_url":null,"is_allow_capacity":"no","name":"การเงินและบัญชี","capacity_unit":null},{"id":"DWaVnOlcBD","icon_url":null,"is_allow_capacity":"no","name":"งานไกล่เกลี่ย","capacity_unit":"คน"},{"id":"ja6Og46hBo","icon_url":null,"is_allow_capacity":"no","name":"งานสารบรรณ","capacity_unit":"คน"},{"id":"nLAW8rXtkq","icon_url":null,"is_allow_capacity":"no","name":"อุธรณ์-ฎีกา","capacity_unit":"คน"},{"id":"pQqE14ju9Z","icon_url":null,"is_allow_capacity":"no","name":"งานบังคับคดี","capacity_unit":"คน"},{"id":"QjnP484f4b","icon_url":null,"is_allow_capacity":"no","name":"ศูนย์หน้าบัลลังก์","capacity_unit":"คน"},{"id":"qOP8d4JtzG","icon_url":null,"is_allow_capacity":"no","name":"ศูนย์นัดความ","capacity_unit":"คน"},{"id":"rQeyAzJSlR","icon_url":null,"is_allow_capacity":"no","name":"เก็บสำนวนแดง","capacity_unit":"คน"},{"id":"wdlamrJSay","icon_url":null,"is_allow_capacity":"no","name":"ศูนย์คัดถ่าย","capacity_unit":"คน"},{"id":"YwkL4raIoV","icon_url":null,"is_allow_capacity":"no","name":"เก็บสำนวนดำ","capacity_unit":"คน"},{"id":"zajm3K9fJz","icon_url":null,"is_allow_capacity":"no","name":"รับฟ้องแพ่ง","capacity_unit":"คน"}]
         */

        private String _id;
        private String name;
        private int index;
        private String brandId;
        private List<ServicesBean> services;

        public String get_id() {
            return _id;
        }

        public void set_id(String _id) {
            this._id = _id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getIndex() {
            return index;
        }

        public void setIndex(int index) {
            this.index = index;
        }

        public String getBrandId() {
            return brandId;
        }

        public void setBrandId(String brandId) {
            this.brandId = brandId;
        }

        public List<ServicesBean> getServices() {
            return services;
        }

        public void setServices(List<ServicesBean> services) {
            this.services = services;
        }

        public static class ServicesBean {
            /**
             * id : 1GVr29PI3K
             * icon_url : null
             * is_allow_capacity : no
             * name : ฝากขัง-หมายค้น-หมายจับ
             * capacity_unit : คน
             */

            private String id;
            private Object icon_url;
            private String is_allow_capacity;
            private String name;
            private String capacity_unit;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public Object getIcon_url() {
                return icon_url;
            }

            public void setIcon_url(Object icon_url) {
                this.icon_url = icon_url;
            }

            public String getIs_allow_capacity() {
                return is_allow_capacity;
            }

            public void setIs_allow_capacity(String is_allow_capacity) {
                this.is_allow_capacity = is_allow_capacity;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getCapacity_unit() {
                return capacity_unit;
            }

            public void setCapacity_unit(String capacity_unit) {
                this.capacity_unit = capacity_unit;
            }
        }
    }

    public static class ConfigsBean {
        /**
         * clientType : local_server
         * allowGroupManage : true
         * logo : null
         * printers : {"logoTop":null,"logoBottom":null}
         * defaultSettings : {"allowReserve":true,"groupManagement":[{"serviceId":"9bx8pxeh8a","maxCapacity":1,"minCapacity":1,"allowBooking":false},{"serviceId":"zajm3K9fJz","minCapacity":1,"maxCapacity":1,"allowBooking":false},{"serviceId":"44po5qLup9","minCapacity":1,"maxCapacity":1,"allowBooking":false},{"serviceId":"YwkL4raIoV","minCapacity":1,"maxCapacity":1,"allowBooking":false},{"serviceId":"rQeyAzJSlR","minCapacity":1,"maxCapacity":1,"allowBooking":false},{"serviceId":"nLAW8rXtkq","minCapacity":1,"maxCapacity":1,"allowBooking":false},{"serviceId":"1GVr29PI3K","minCapacity":1,"maxCapacity":1,"allowBooking":false},{"serviceId":"wdlamrJSay","minCapacity":1,"maxCapacity":1,"allowBooking":false},{"serviceId":"QjnP484f4b","minCapacity":1,"maxCapacity":1,"allowBooking":false},{"serviceId":"qOP8d4JtzG","minCapacity":1,"maxCapacity":1,"allowBooking":false},{"serviceId":"1GVr2P6hEm","minCapacity":1,"maxCapacity":1,"allowBooking":false},{"serviceId":"pQqE14ju9Z","minCapacity":1,"maxCapacity":1,"allowBooking":false},{"serviceId":"ja6Og46hBo","minCapacity":1,"maxCapacity":1,"allowBooking":false},{"serviceId":"DWaVnOlcBD","minCapacity":1,"maxCapacity":1,"allowBooking":false}]}
         * awsS3 : {"S3_BUCKET_NAME":"biz.cello.db","S3_KEY_DIRECTORY":"data/"}
         * isHoldable : true
         * holdExpireSecond : 60
         */

        private String clientType;
        private boolean allowGroupManage;
        private Object logo;
        private PrintersBean printers;
        private DefaultSettingsBean defaultSettings;
        private AwsS3Bean awsS3;
        private boolean isHoldable;
        private int holdExpireSecond;

        public String getClientType() {
            return clientType;
        }

        public void setClientType(String clientType) {
            this.clientType = clientType;
        }

        public boolean isAllowGroupManage() {
            return allowGroupManage;
        }

        public void setAllowGroupManage(boolean allowGroupManage) {
            this.allowGroupManage = allowGroupManage;
        }

        public Object getLogo() {
            return logo;
        }

        public void setLogo(Object logo) {
            this.logo = logo;
        }

        public PrintersBean getPrinters() {
            return printers;
        }

        public void setPrinters(PrintersBean printers) {
            this.printers = printers;
        }

        public DefaultSettingsBean getDefaultSettings() {
            return defaultSettings;
        }

        public void setDefaultSettings(DefaultSettingsBean defaultSettings) {
            this.defaultSettings = defaultSettings;
        }

        public AwsS3Bean getAwsS3() {
            return awsS3;
        }

        public void setAwsS3(AwsS3Bean awsS3) {
            this.awsS3 = awsS3;
        }

        public boolean isIsHoldable() {
            return isHoldable;
        }

        public void setIsHoldable(boolean isHoldable) {
            this.isHoldable = isHoldable;
        }

        public int getHoldExpireSecond() {
            return holdExpireSecond;
        }

        public void setHoldExpireSecond(int holdExpireSecond) {
            this.holdExpireSecond = holdExpireSecond;
        }

        public static class PrintersBean {
            /**
             * logoTop : null
             * logoBottom : null
             */

            private Object logoTop;
            private Object logoBottom;

            public Object getLogoTop() {
                return logoTop;
            }

            public void setLogoTop(Object logoTop) {
                this.logoTop = logoTop;
            }

            public Object getLogoBottom() {
                return logoBottom;
            }

            public void setLogoBottom(Object logoBottom) {
                this.logoBottom = logoBottom;
            }
        }

        public static class DefaultSettingsBean {
            /**
             * allowReserve : true
             * groupManagement : [{"serviceId":"9bx8pxeh8a","maxCapacity":1,"minCapacity":1,"allowBooking":false},{"serviceId":"zajm3K9fJz","minCapacity":1,"maxCapacity":1,"allowBooking":false},{"serviceId":"44po5qLup9","minCapacity":1,"maxCapacity":1,"allowBooking":false},{"serviceId":"YwkL4raIoV","minCapacity":1,"maxCapacity":1,"allowBooking":false},{"serviceId":"rQeyAzJSlR","minCapacity":1,"maxCapacity":1,"allowBooking":false},{"serviceId":"nLAW8rXtkq","minCapacity":1,"maxCapacity":1,"allowBooking":false},{"serviceId":"1GVr29PI3K","minCapacity":1,"maxCapacity":1,"allowBooking":false},{"serviceId":"wdlamrJSay","minCapacity":1,"maxCapacity":1,"allowBooking":false},{"serviceId":"QjnP484f4b","minCapacity":1,"maxCapacity":1,"allowBooking":false},{"serviceId":"qOP8d4JtzG","minCapacity":1,"maxCapacity":1,"allowBooking":false},{"serviceId":"1GVr2P6hEm","minCapacity":1,"maxCapacity":1,"allowBooking":false},{"serviceId":"pQqE14ju9Z","minCapacity":1,"maxCapacity":1,"allowBooking":false},{"serviceId":"ja6Og46hBo","minCapacity":1,"maxCapacity":1,"allowBooking":false},{"serviceId":"DWaVnOlcBD","minCapacity":1,"maxCapacity":1,"allowBooking":false}]
             */

            private boolean allowReserve;
            private List<GroupManagementBean> groupManagement;

            public boolean isAllowReserve() {
                return allowReserve;
            }

            public void setAllowReserve(boolean allowReserve) {
                this.allowReserve = allowReserve;
            }

            public List<GroupManagementBean> getGroupManagement() {
                return groupManagement;
            }

            public void setGroupManagement(List<GroupManagementBean> groupManagement) {
                this.groupManagement = groupManagement;
            }

            public static class GroupManagementBean {
                /**
                 * serviceId : 9bx8pxeh8a
                 * maxCapacity : 1
                 * minCapacity : 1
                 * allowBooking : false
                 */

                private String serviceId;
                private int maxCapacity;
                private int minCapacity;
                private boolean allowBooking;

                public String getServiceId() {
                    return serviceId;
                }

                public void setServiceId(String serviceId) {
                    this.serviceId = serviceId;
                }

                public int getMaxCapacity() {
                    return maxCapacity;
                }

                public void setMaxCapacity(int maxCapacity) {
                    this.maxCapacity = maxCapacity;
                }

                public int getMinCapacity() {
                    return minCapacity;
                }

                public void setMinCapacity(int minCapacity) {
                    this.minCapacity = minCapacity;
                }

                public boolean isAllowBooking() {
                    return allowBooking;
                }

                public void setAllowBooking(boolean allowBooking) {
                    this.allowBooking = allowBooking;
                }
            }
        }

        public static class AwsS3Bean {
            /**
             * S3_BUCKET_NAME : biz.cello.db
             * S3_KEY_DIRECTORY : data/
             */

            private String S3_BUCKET_NAME;
            private String S3_KEY_DIRECTORY;

            public String getS3_BUCKET_NAME() {
                return S3_BUCKET_NAME;
            }

            public void setS3_BUCKET_NAME(String S3_BUCKET_NAME) {
                this.S3_BUCKET_NAME = S3_BUCKET_NAME;
            }

            public String getS3_KEY_DIRECTORY() {
                return S3_KEY_DIRECTORY;
            }

            public void setS3_KEY_DIRECTORY(String S3_KEY_DIRECTORY) {
                this.S3_KEY_DIRECTORY = S3_KEY_DIRECTORY;
            }
        }
    }

    public static class DeviceSettingsBean {
        /**
         * client : {"osVersion":"10.0.14393","deviceType":"Windows_NT","appVersion":"","hostName":"DESKTOP-UO4BHH1"}
         * connections : {"tv":"","printer":""}
         */

        private ClientBean client;
        private ConnectionsBean connections;

        public ClientBean getClient() {
            return client;
        }

        public void setClient(ClientBean client) {
            this.client = client;
        }

        public ConnectionsBean getConnections() {
            return connections;
        }

        public void setConnections(ConnectionsBean connections) {
            this.connections = connections;
        }

        public static class ClientBean {
            /**
             * osVersion : 10.0.14393
             * deviceType : Windows_NT
             * appVersion :
             * hostName : DESKTOP-UO4BHH1
             */

            private String osVersion;
            private String deviceType;
            private String appVersion;
            private String hostName;

            public String getOsVersion() {
                return osVersion;
            }

            public void setOsVersion(String osVersion) {
                this.osVersion = osVersion;
            }

            public String getDeviceType() {
                return deviceType;
            }

            public void setDeviceType(String deviceType) {
                this.deviceType = deviceType;
            }

            public String getAppVersion() {
                return appVersion;
            }

            public void setAppVersion(String appVersion) {
                this.appVersion = appVersion;
            }

            public String getHostName() {
                return hostName;
            }

            public void setHostName(String hostName) {
                this.hostName = hostName;
            }
        }

        public static class ConnectionsBean {
            /**
             * tv :
             * printer :
             */

            private String tv;
            private String printer;

            public String getTv() {
                return tv;
            }

            public void setTv(String tv) {
                this.tv = tv;
            }

            public String getPrinter() {
                return printer;
            }

            public void setPrinter(String printer) {
                this.printer = printer;
            }
        }
    }

    public static class CreatedAtBean {
        /**
         * $date : 1482146491351
         */

        private long $date;

        public long get$date() {
            return $date;
        }

        public void set$date(long $date) {
            this.$date = $date;
        }
    }
}
