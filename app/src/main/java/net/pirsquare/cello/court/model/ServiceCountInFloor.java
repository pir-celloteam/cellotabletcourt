package net.pirsquare.cello.court.model;

import net.pirsquare.cello.court.utils.Constant;

/**
 * Created by Nut on 12/19/16.
 */

public enum ServiceCountInFloor {
    F1("floor_1", Constant.FIRST_FLOOR, Constant.START_FIRST_FLOOR), F2("floor_2", Constant.SECOND_FLOOR, Constant.START_SECOND_FLOOR), F3("floor_3", Constant.THIRD_FLOOR, Constant.START_THIRD_FLOOR);

    private String floor;
    private int i;
    private int start;

    ServiceCountInFloor(String floor, int i, int start) {
        this.floor = floor;
        this.i = i;
        this.start = start;
    }

    public int getI() {
        return i;
    }

    public int getStart() {
        return start;
    }

    public static int getCountNumber(int i) {
        switch (i) {
            case 1:
                return F1.getI();
            case 2:
                return F2.getI();
            case 3:
                return F3.getI();
        }
        return 0;
    }

    public static int getStartNumber(int i) {
        switch (i) {
            case 1:
                return F1.getStart();
            case 2:
                return F2.getStart();
            case 3:
                return F3.getStart();
        }
        return 0;
    }
}
