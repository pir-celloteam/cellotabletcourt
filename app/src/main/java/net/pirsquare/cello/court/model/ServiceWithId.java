package net.pirsquare.cello.court.model;

/**
 * Created by kung on 12/2/16.
 */

public class ServiceWithId {
    private String name;
    private Service service;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}
