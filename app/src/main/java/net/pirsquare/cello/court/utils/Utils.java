package net.pirsquare.cello.court.utils;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.provider.Settings;
import android.util.Log;

import net.pirsquare.cello.court.manager.BrandManager;
import net.pirsquare.cello.court.manager.Contextor;
import net.pirsquare.cello.court.model.QueueCreated;

/**
 * Created by nuuneoi on 10/16/2014.
 */
public class Utils {

    private static Utils instance;

    public static Utils getInstance() {
        if (instance == null)
            instance = new Utils();
        return instance;
    }

    private Context mContext;

    private Utils() {
        mContext = Contextor.getInstance().getContext();
    }

    public static String getCode(QueueCreated queue) {
        return Constant.API_QR_URL + BrandManager.getInstance().getBrandData().getBranch().get_id() + "?q=" + queue.get_id();
    }

    public String getDeviceId() {
        return Settings.Secure.getString(mContext.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    public String getVersionName() {
        try {
            PackageInfo pInfo = mContext.getPackageManager().getPackageInfo(mContext.getPackageName(), 0);
            return pInfo.versionName;
        } catch (Exception e) {
            return "1.0";
        }
    }

    public static boolean validIP(String ip) {

        try {
            if (ip == null || ip.isEmpty()) {
                Log.e("valid", "stage 1");
                return false;
            }

            String[] parts = ip.split("\\.");
            if (parts.length != 4) {
                Log.e("valid", "stage 2");

                return false;
            }

            for (String s : parts) {
                int i = Integer.parseInt(s);
                if ((i < 0) || (i > 255)) {
                    Log.e("valid", "stage 3");

                    return false;
                }
            }
            if (ip.endsWith(".")) {
                Log.e("valid", "stage 4");

                return false;
            }

            return true;
        } catch (NumberFormatException nfe) {
            return false;
        }
    }

}
