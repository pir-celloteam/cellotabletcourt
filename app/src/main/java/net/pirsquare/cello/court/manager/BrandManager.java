package net.pirsquare.cello.court.manager;

import android.content.Context;

import net.pirsquare.cello.court.model.BrandData;

/**
 * Created by Nut on 12/19/2016.
 */
public class BrandManager {

    private static BrandManager instance;
    private BrandData brandData;

    public static BrandManager getInstance() {
        if (instance == null)
            instance = new BrandManager();
        return instance;
    }

    private Context mContext;

    private BrandManager() {
        mContext = Contextor.getInstance().getContext();
    }

    public BrandData getBrandData() {
        return brandData;
    }

    public void setBrandData(BrandData brandData) {
        this.brandData = brandData;
    }
}
