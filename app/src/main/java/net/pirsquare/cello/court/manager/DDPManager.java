package net.pirsquare.cello.court.manager;

import android.util.Log;

import com.orhanobut.logger.Logger;

import net.pirsquare.cello.court.activity.MainActivity;
import net.pirsquare.cello.court.api.GsonHelper;
import net.pirsquare.cello.court.model.BrandData;
import net.pirsquare.cello.court.model.QueueCreated;
import net.pirsquare.cello.court.model.Service;
import net.pirsquare.cello.court.model.ServiceWithId;
import net.pirsquare.cello.court.utils.Constant;

import org.codehaus.jackson.map.util.JSONPObject;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import im.delight.android.ddp.Meteor;
import im.delight.android.ddp.MeteorCallback;
import im.delight.android.ddp.ResultListener;
import im.delight.android.ddp.SubscribeListener;
import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subjects.BehaviorSubject;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by kampolleelaporn on 3/28/16.
 */
public class DDPManager implements MeteorCallback {

    private MainActivity mainActivity;
    private static DDPManager mInstance;
    private static Meteor mMeteor;
    private static String TAG = "METERO";
    private String subscriptionId;

    private BehaviorSubject<Boolean> observeStatusMeteor;


    private ArrayList<Map<String, Object>> listStackQueue;
    private HashMap<String, Object> queueHashmap;

    private static ConnectServerListener listener;

    private CompositeSubscription observable;
    private boolean isConnect;
    private ReconnectListener reconnectListener;
    private boolean isClose;

    public void init(MainActivity context, ConnectServerListener mListener) {
        this.mainActivity = context;
        listener = mListener;
        Meteor.setLoggingEnabled(true);
        mMeteor = new Meteor(context, Constant.METEOR_SERVER_URL);
        mMeteor.addCallback(mInstance);
        mMeteor.connect();
    }

    public static DDPManager getInstance() {
        if (mInstance == null)
            mInstance = new DDPManager();
        return mInstance;
    }


    public BehaviorSubject<Boolean> subscribeStatus() {
        BehaviorSubject<Boolean> observe = BehaviorSubject.create(mMeteor.isConnected());
        observe.subscribeOn(Schedulers.io())
                .distinctUntilChanged()
                .observeOn(AndroidSchedulers.mainThread());
        observeStatusMeteor = observe;
        return observe;
    }

    public void postStatusMeteor(boolean status) {
        if (observeStatusMeteor != null)
            observeStatusMeteor.onNext(status);
    }


    public boolean isConnect() {
        return isConnect;
    }


    @Override
    public void onConnect(boolean signedInAutomatically) {
        isConnect = true;
        Log.e(TAG, "Connected");
        Log.e(TAG, "Is logged in: " + mMeteor.isLoggedIn());
        Log.e(TAG, "User ID: " + mMeteor.getUserId());
        if (signedInAutomatically) {
            if (observable != null) {
                observable.unsubscribe();
            }
        }

        if (reconnectListener != null) {
            reconnectListener.reconnectSuccess();
            Logger.e("recon success from method connect");
            reconnectListener = null;
        }

        subscribe();
    }


    private void subscribe() {
        Map<String, Object> brandParam = new HashMap<>();
        mMeteor.subscribe("initializes", new Object[]{brandParam}, new SubscribeListener() {
            @Override
            public void onSuccess() {
                Log.e(TAG, "Subscript initializes Success");
            }

            @Override
            public void onError(String error, String reason, String details) {

            }
        });

        Map<String, Object> param = new HashMap<>();
        subscriptionId = mMeteor.subscribe("services.list", new Object[]{param}, new SubscribeListener() {
            @Override
            public void onSuccess() {
                Log.e(TAG, "Subscript services.list Success");
                if (listener != null)
                    listener.onSuccess();
                postStatusMeteor(true);
            }

            @Override
            public void onError(String s, String s1, String s2) {
                Log.e(TAG, "Subscript Fail");
                if (listener != null)
                    listener.onFail();
                postStatusMeteor(false);
            }
        });

    }

    public void onReconnect(ReconnectListener reconnectListener) {
        if (!isConnect()) {
            mMeteor.reconnect();
            this.reconnectListener = reconnectListener;
        }
    }

    @Override
    public void onDisconnect() {
        Log.e(TAG, "Server Disconnect");
        isConnect = false;

        postStatusMeteor(false);

        if (!isClose) {
            if (listener != null)
                listener.onFail();
            if (reconnectListener != null) {
                reconnectListener.reconnectFail();
                reconnectListener = null;
            }
        }
    }

    @Override
    public void onException(Exception e) {
        e.printStackTrace();
        Log.e(TAG, "Exception " + e.getMessage() + " : ");
        mMeteor.disconnect();
        if (e != null) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDataAdded(String collectionName, String documentID, String newValuesJson) {
//        Logger.d(" added to <" + collectionName + "> in <" + documentID + ">" + "    Added: ");
        Log.e(TAG, " added to <" + collectionName + "> in <" + documentID + ">" + "    Added: ");
        Logger.json(newValuesJson);
//        Log.e(TAG, "    Added: " + newValuesJson);
        try {

            JSONObject json = new JSONObject(newValuesJson);
            if (json.has("branch")) {
                BrandData brandData = GsonHelper.getGson().fromJson(newValuesJson, BrandData.class);
                BrandManager.getInstance().setBrandData(brandData);

            } else {
                if (!ServiceManager.getInstance().isHadService()) {
                    Service service = GsonHelper.getGson().fromJson(newValuesJson, Service.class);
                    ServiceWithId serviceWithId = new ServiceWithId();
                    serviceWithId.setName(documentID);
                    serviceWithId.setService(service);

                    ServiceManager.getInstance().setServiceWithIds(serviceWithId);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDataChanged(String s, String s1, String s2, String s3) {
        Log.e(TAG, "  changed in <" + s + "> in <" + s1 + ">");
        Log.e(TAG, "    Updated: " + s2);
        Log.e(TAG, "    Removed: " + s3);

    }

    @Override
    public void onDataRemoved(String s, String s1) {
        Log.e(TAG, "Queue removed from <" + s + "> in document <" + s1 + ">");
    }

    public void close() {
        Log.e(TAG, "Meteor Closed");
        isClose = true;
        if (mMeteor.isConnected()) {
            mMeteor.disconnect();
            mMeteor.removeCallback(this);
            mInstance = null;
        }
        if (observable != null)
            observable.unsubscribe();
    }

    public void requestQueue(String serviceID, CreateQueueListener listener) {
        Map<String, Object> param = new HashMap<>();

        Map<String, Object> service = new HashMap<>();
        service.put("id", serviceID);
        param.put("service", service);

        mMeteor.call("queues.create", new Object[]{param}, new ResultListener() {
            @Override
            public void onSuccess(String result) {
                Log.e(TAG, "create Success " + result);

                QueueCreated queueCreated = GsonHelper.getGson().fromJson(result, QueueCreated.class);
                listener.onSuccess(queueCreated);
            }

            @Override
            public void onError(String error, String reason, String details) {
                Log.e(TAG, "create Error : " + error + "  Reason : " + reason + " Detail : " + details);
                listener.onFail();
            }
        });
    }

    public interface ConnectServerListener {
        void onSuccess();

        void onFail();
    }

    public interface ReconnectListener {
        void reconnectSuccess();

        void reconnectFail();
    }

    public interface CreateQueueListener {
        void onSuccess(QueueCreated queueCreated);

        void onFail();
    }
}