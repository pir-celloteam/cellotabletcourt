package net.pirsquare.cello.court.adapter;

import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import net.pirsquare.cello.court.R;
import net.pirsquare.cello.court.model.ServiceWithId;
import net.pirsquare.cello.court.utils.ConvertDesity;
import net.pirsquare.cello.court.utils.ScreenUtils;

import java.util.ArrayList;

/**
 * Created by Nut on 12/2/16.
 */

public class ServiceAdapter extends RecyclerView.Adapter<ServiceAdapter.MyViewHolder> {
    private ArrayList<ServiceWithId> listService;
    private OnClickButton onClickButton;
    private FragmentActivity activity;

    public interface OnClickButton {
        void onClickServiceButton(ServiceWithId serviceWithId);
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public ServiceAdapter(ArrayList<ServiceWithId> myDataset, FragmentActivity activity, OnClickButton onClickButton) {
        this.listService = myDataset;
        this.activity = activity;
        this.onClickButton = onClickButton;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public View btnService;
        public TextView tvService;

        public MyViewHolder(View v) {
            super(v);
            btnService = v.findViewById(R.id.btn_service);
            tvService = (TextView) v.findViewById(R.id.tv_service);
        }

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.service_button, parent, false);
//        float screenHeight = ScreenUtils.getInstance().getScreenHeight();
//
//        ViewGroup.LayoutParams layoutParams = itemView.getLayoutParams();
//        layoutParams.height = (int) (ConvertDesity.convertPixelsToDp(screenHeight, activity) - 130) / 8;
//
//        itemView.setLayoutParams(layoutParams);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        holder.tvService.setText(listService.get(position).getService().getName());
        holder.btnService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickButton.onClickServiceButton(listService.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return listService.size();
    }

}
