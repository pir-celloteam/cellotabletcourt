package net.pirsquare.cello.court.fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.orhanobut.logger.Logger;

import net.pirsquare.cello.court.MainApplication;
import net.pirsquare.cello.court.R;
import net.pirsquare.cello.court.activity.MainActivity;
import net.pirsquare.cello.court.adapter.ServiceAdapter;
import net.pirsquare.cello.court.manager.DDPManager;
import net.pirsquare.cello.court.manager.PrinterManager;
import net.pirsquare.cello.court.manager.ServiceManager;
import net.pirsquare.cello.court.model.QueueCreated;
import net.pirsquare.cello.court.model.ServiceCountInFloor;
import net.pirsquare.cello.court.model.ServiceFloor;
import net.pirsquare.cello.court.model.ServiceWithId;
import net.pirsquare.cello.court.utils.DateHelper;
import net.pirsquare.cello.court.utils.ProgressDialogBox;
import net.pirsquare.cello.court.utils.ToastDialog;
import net.pirsquare.cello.court.utils.Utils;
import net.pirsquare.starprintersdk.PirSquarePrinter;

import org.joda.time.DateTime;

import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;


/**
 * Created by Nut on 12/6/2016.
 */
@SuppressWarnings("unused")
public class CallServiceFragment extends Fragment implements ServiceAdapter.OnClickButton {

    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.Adapter mAdapter;
    private ProgressDialogBox dialogBox;
    private ToastDialog toastDialog;

    private ArrayList<ServiceWithId> listService, listServiceModel;
    private ServiceFloor floor;
    private int numberInFloor, numberStartFloor;

    public CallServiceFragment() {
        super();
    }

    @SuppressWarnings("unused")
    public static CallServiceFragment newInstance(ServiceFloor floor) {
        CallServiceFragment fragment = new CallServiceFragment();
        Bundle args = new Bundle();
        args.putSerializable("floor", floor);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init(savedInstanceState);

        if (savedInstanceState != null)
            onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_call_service, container, false);
        initInstances(rootView, savedInstanceState);
        return rootView;
    }

    private void init(Bundle savedInstanceState) {
        // Init Fragment level's variable(s) here
        floor = (ServiceFloor) getArguments().getSerializable("floor");
        if (floor.getI() != -1) {
            numberInFloor = ServiceCountInFloor.getCountNumber(floor.getI());
            numberStartFloor = ServiceCountInFloor.getStartNumber(floor.getI());
        }
        dialogBox = new ProgressDialogBox(getActivity());
        toastDialog = new ToastDialog(getActivity());
    }

    @SuppressWarnings("UnusedParameters")
    private void initInstances(View rootView, Bundle savedInstanceState) {
        // Init 'View' instance(s) with rootView.findViewById here
        listService = new ArrayList<>();
        listServiceModel = new ArrayList<>();
        listServiceModel.addAll(ServiceManager.getInstance().getServiceWithIds());

        for (int i = 0; i < numberInFloor; i++) {
            listService.add(i, listServiceModel.get(numberStartFloor + i));
        }

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new ServiceAdapter(listService, getActivity(), this);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();

    }


    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    /*
     * Save Instance State Here
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save Instance State here
    }

    /*
     * Restore Instance State Here
     */
    @SuppressWarnings("UnusedParameters")
    private void onRestoreInstanceState(Bundle savedInstanceState) {
        // Restore Instance State here
    }


    @Override
    public void onClickServiceButton(ServiceWithId serviceWithId) {
        if (PrinterManager.getInstance().isAutoPrint())
            dialogBox.showPrintingDialog("Waiting", "Printer is printing");

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                DDPManager.getInstance().requestQueue(serviceWithId.getName(), new DDPManager.CreateQueueListener() {
                    @Override
                    public void onSuccess(QueueCreated queueCreated) {
                        Toast.makeText(getActivity(), "Create Queue Success", Toast.LENGTH_SHORT).show();

                        if (PrinterManager.getInstance().isAutoPrint()) {

                            PrinterManager.getInstance().printQueue(DateHelper.formatFully.print((new DateTime(queueCreated.getCreatedAt().get$date()))),
                                    String.format("%s%s", queueCreated.getGroup(), queueCreated.getIndex()), Utils.getCode(queueCreated), queueCreated.getRemain(),
                                    queueCreated.getService().getName(), queueCreated.getRefCode(), new PrinterManager.PrinterListener() {
                                        @Override
                                        public void onPrintSuccess() {
                                            dialogBox.hideProgressDialog();
                                            toastDialog.showToastNew("onPrintSuccess");
                                        }

                                        @Override
                                        public void onPrintFail() {
                                            dialogBox.hideProgressDialog();
                                            toastDialog.showToastNew("onPrintFail");
                                        }
                                    });
                        }
                    }

                    @Override
                    public void onFail() {
                        Toast.makeText(getActivity(), "Create Queue Failed", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }, 750);
    }
}
