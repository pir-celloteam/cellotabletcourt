package net.pirsquare.cello.court.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import net.pirsquare.cello.court.fragment.CallServiceFragment;
import net.pirsquare.cello.court.model.ServiceFloor;

/**
 * Created by Nut on 12/6/16.
 */

public class FragmentPagerAdapter extends android.support.v4.app.FragmentPagerAdapter {

    private CallServiceFragment callServiceFloor1;
    private CallServiceFragment callServiceFloor2;
    private CallServiceFragment callServiceFloor3;

    public FragmentPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        switch (position) {
            case 0:
                if (callServiceFloor1 == null)
                    callServiceFloor1 = CallServiceFragment.newInstance(ServiceFloor.F1);
                fragment = callServiceFloor1;
                break;
            case 1:
                if (callServiceFloor2 == null)
                    callServiceFloor2 = CallServiceFragment.newInstance(ServiceFloor.F2);
                fragment = callServiceFloor2;
                break;
            case 2:
                if (callServiceFloor3 == null)
                    callServiceFloor3 = CallServiceFragment.newInstance(ServiceFloor.F3);
                fragment = callServiceFloor3;
                break;

        }
        return fragment;
    }

    @Override
    public int getCount() {
        return 3;
    }
}
