package net.pirsquare.cello.court.manager;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import net.glxn.qrgen.android.QRCode;
import net.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import net.pirsquare.cello.court.utils.DateHelper;
import net.pirsquare.starprintersdk.PirSquarePrinter;

import org.joda.time.DateTime;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subjects.BehaviorSubject;

/**
 * Created by Nut on 12/19/16.
 */
public class PrinterManager {

    private static PrinterManager mInstance;

    private Context mContext;
    private String ip;

    private boolean isReady;
    private boolean isAutoPrint;

    public boolean isReady() {
        return isReady;
    }

    public void setReady(boolean ready) {
        isReady = ready;
    }


    public boolean isAutoPrint() {
        return isAutoPrint;
    }

    public void setAutoPrint(boolean autoPrint) {
        isAutoPrint = autoPrint;
    }

    public PrinterManager() {

    }

    public void init(String ip, Context mContext) {
        this.mContext = mContext;
        this.ip = ip;
        PirSquarePrinter.createInstance(ip, mContext);

        RxBus.getInstance().send(new PrinterEvent(true));
    }

    public static PrinterManager getInstance() {
        if (mInstance == null)
            mInstance = new PrinterManager();
        return mInstance;
    }

    public BehaviorSubject<Boolean> subscribeStatus() {
        BehaviorSubject<Boolean> observe = BehaviorSubject.create(isReady());
        observe.subscribeOn(Schedulers.io())
                .distinctUntilChanged()
                .observeOn(AndroidSchedulers.mainThread());
        return observe;
    }

    public void printQueue(String time_stamp, String queue_id,
                           String qr, int queue_remain, String serviceName, String refCode, PrinterListener printerListener) {

        PirSquarePrinter.getInstance().printQueue(getView(time_stamp
                , queue_id
                , String.valueOf(queue_remain)
                , qr, serviceName, refCode), 408, new PirSquarePrinter.PrinterListener() {
            @Override
            public void onPrintSuccess() {
                if (printerListener != null)
                    printerListener.onPrintSuccess();
                RxBus.getInstance().send(new PrinterEvent(true));
            }

            @Override
            public void onPrintFail() {
                if (printerListener != null)
                    printerListener.onPrintFail();
                RxBus.getInstance().send(new PrinterEvent(false));
            }
        });
    }

    public void checkPrint(ConnectPrinterListener listener) {
        PirSquarePrinter.getInstance().connectPrinter(ip, new PirSquarePrinter.ConnectPrinterListener() {
            @Override
            public void onConnectSuccess() {
                listener.onConnectSuccess();
                RxBus.getInstance().send(new PrinterEvent(true));

            }

            @Override
            public void onConnectFail() {
                listener.onConnectFail();
                RxBus.getInstance().send(new PrinterEvent(false));

            }
        });
    }

    public void reConnectPrinter(ConnectPrinterListener listener) {
        PirSquarePrinter.createInstance(ip, mContext);
        PirSquarePrinter.getInstance().connectPrinter(ip, new PirSquarePrinter.ConnectPrinterListener() {
            @Override
            public void onConnectSuccess() {
                listener.onConnectSuccess();
                RxBus.getInstance().send(new PrinterEvent(true));

            }

            @Override
            public void onConnectFail() {
                listener.onConnectFail();
                RxBus.getInstance().send(new PrinterEvent(false));

            }
        });
    }

    public void disconnect() {
        mInstance = null;
    }

    public static class PrinterEvent {
        public boolean isConnect;

        public PrinterEvent(boolean isConnect) {
            this.isConnect = isConnect;
        }
    }

    public Bitmap getView(String timeStamp, String queueId, String currentQueueNumb, String qrString, String serviceName, String refCode) {
        LinearLayout queueReceive = (LinearLayout) LayoutInflater.from(mContext).inflate(net.pirsquare.cello.court.R.layout.printer_recieve, null);

//        ImageView imageTop = (ImageView) queueReceive.findViewById(net.pirsquare.cello.court.R.id.image_logo_top);
//        ImageView imageBottom = (ImageView) queueReceive.findViewById(net.pirsquare.cello.court.R.id.image_logo_bottom);

        TextView textviewBranchName = (TextView) queueReceive.findViewById(net.pirsquare.cello.court.R.id.textview_branch_name);
        TextView textviewTime = (TextView) queueReceive.findViewById(net.pirsquare.cello.court.R.id.textview_date);
        TextView textviewQueueNumber = (TextView) queueReceive.findViewById(net.pirsquare.cello.court.R.id.textview_queue_number);
        ImageView imageQr = (ImageView) queueReceive.findViewById(net.pirsquare.cello.court.R.id.imageview_qr);
        TextView textviewWait = (TextView) queueReceive.findViewById(net.pirsquare.cello.court.R.id.textview_queue_wating);
        TextView textviewServiceName = (TextView) queueReceive.findViewById(net.pirsquare.cello.court.R.id.textview_service_name);
        TextView textviewRefCode = (TextView) queueReceive.findViewById(net.pirsquare.cello.court.R.id.textview_ref_code);
        Bitmap bitmapQR = QRCode.from(qrString).withErrorCorrection(ErrorCorrectionLevel.L).withSize(400, 400).bitmap();
        imageQr.setImageBitmap(bitmapQR);

        textviewBranchName.setText(BrandManager.getInstance().getBrandData().getBranch().getName());
        textviewServiceName.setText(serviceName);
        textviewQueueNumber.setText(String.format("%s", queueId));
        textviewWait.setText(String.format("รออีก %s คิว", currentQueueNumb));
        textviewTime.setText(DateHelper.formatShowInQueuePaper.print(new DateTime(timeStamp)));
        textviewRefCode.setText(String.format("LINE Code : @%s", refCode));

        queueReceive.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        queueReceive.invalidate();
        Bitmap b = Bitmap.createBitmap(queueReceive.getMeasuredWidth(), queueReceive.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(b);
        queueReceive.layout(0, 0, queueReceive.getMeasuredWidth(), queueReceive.getMeasuredHeight());
        queueReceive.draw(c);
        int width = b.getWidth() * 2;
        int height = b.getHeight() * 2;
        b = Bitmap.createScaledBitmap(b, width, height, false);
        return b;
    }

    public interface PrinterListener {
        void onPrintSuccess();

        void onPrintFail();
    }

    public interface ConnectPrinterListener {
        void onConnectSuccess();

        void onConnectFail();
    }
}
