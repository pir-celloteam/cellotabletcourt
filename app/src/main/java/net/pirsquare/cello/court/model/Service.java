package net.pirsquare.cello.court.model;

/**
 * Created by Nut on 12/2/16.
 */

public class Service {


    /**
     * name : ศูนย์หน้าบัลลังก์
     * groupName : I
     * maxCapacity : 1
     * minCapacity : 1
     * allowBooking : false
     * createdAt : {"$date":1482141966011}
     */

    private String name;
    private String groupName;
    private int maxCapacity;
    private int minCapacity;
    private boolean allowBooking;
    private CreatedAtBean createdAt;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public int getMaxCapacity() {
        return maxCapacity;
    }

    public void setMaxCapacity(int maxCapacity) {
        this.maxCapacity = maxCapacity;
    }

    public int getMinCapacity() {
        return minCapacity;
    }

    public void setMinCapacity(int minCapacity) {
        this.minCapacity = minCapacity;
    }

    public boolean isAllowBooking() {
        return allowBooking;
    }

    public void setAllowBooking(boolean allowBooking) {
        this.allowBooking = allowBooking;
    }

    public CreatedAtBean getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(CreatedAtBean createdAt) {
        this.createdAt = createdAt;
    }

    public static class CreatedAtBean {
        /**
         * $date : 1482141966011
         */

        private long $date;

        public long get$date() {
            return $date;
        }

        public void set$date(long $date) {
            this.$date = $date;
        }
    }
}
