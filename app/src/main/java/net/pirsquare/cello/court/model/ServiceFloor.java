package net.pirsquare.cello.court.model;

/**
 * Created by kung on 12/6/16.
 */

public enum ServiceFloor {
    F1("floor_1", 1), F2("floor_2", 2), F3("floor_3", 3), F4("floor_4", 4), F5("floor_5", 5);

    private String floor;
    private int i;

    ServiceFloor(String floor, int i) {
        this.floor = floor;
        this.i = i;
    }

    public String getFloor() {
        return floor;
    }

    public int getI() {
        return i;
    }

    public static ServiceFloor getType(int i) {
        switch (i) {
            case 1:
                return F1;
            case 2:
                return F2;
            case 3:
                return F3;
            case 4:
                return F4;
            case 5:
                return F5;

        }
        return null;
    }
}
