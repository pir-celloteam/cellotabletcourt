package net.pirsquare.qhappy.QhappyServer.ATV.model;

/**
 * Created by Michael on 07/10/2014.
 */
public class BlockModel {


    public String get_group_id() {
        return _group_id;
    }

    public int get_counter_number() {
        return _counter_number;
    }

    private String _group_id = "";

    public int get_queue_id() {
        return _queue_id;
    }

    private int _queue_id = 0;

    private int _counter_number = 0;

    public BlockModel() {
    }

    public void set(String group_id, int queue_id, int counter_number) {
        _group_id = group_id;
        _queue_id = queue_id;
        _counter_number = counter_number;
    }

    public String toLastQueue() {
        if (_group_id.equals("")) {
            return "-";
        } else {
            return _group_id + "_" + _queue_id + "_" + _counter_number;
        }
    }

    public String toLastGroup() {
        if (_group_id.equals("")) {
            return "-";
        } else {
            return _group_id + "_" + _queue_id + "_" + _counter_number;
        }
    }

    public boolean isSame(String group_id, int queue_id, int counter_number) {
        boolean same_group_id = _group_id.equals(group_id);
        boolean same_queue_id = _queue_id == queue_id;
        boolean same_counter_number = _counter_number == counter_number;

        return (same_group_id && same_queue_id && same_counter_number);
    }
    
    public String toString()
    {
      return _group_id + "_" + _queue_id + "_" + _counter_number;
    }
    
    public static BlockModel fromString(String s)
    {
      BlockModel bm = new BlockModel();
      
      String[] data = s.split("_");
      
      bm.set(data[0], Integer.parseInt(data[1]), Integer.parseInt(data[2]));
      
      return bm;
    }
}
