package net.pirsquare.qhappy.QhappyServer.QueueMachine;

import android.util.Log;

import net.pirsquare.qhappy.QhappyServer.SocketService.ServerSocket;
import net.pirsquare.qhappy.QhappyServer.StatusBar.StatusBarController;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class QueueMachineController {
	private static QueueMachineController mInstance;

	private ServerSocket mServerSocket;
	private String mRoom;
	public static boolean isFiFo = false;

	// singleton
	public static QueueMachineController instance(ServerSocket socket,
			String room) {
		if (mInstance == null) {
			return mInstance = new QueueMachineController(socket, room);
		} else {
			return mInstance;
		}
	}

	public QueueMachineController(ServerSocket socket, String room) {

		mServerSocket = socket;
		mRoom = room;
	}

	// CORE
	private final ArrayList<String> fifo_message = new ArrayList<String>();

	public void sendRemaining() {
		if (mServerSocket.countInChannel(mRoom) <= 0)
			return;

		while (!fifo_message.isEmpty()) {
			if (StatusBarController.isEnable(StatusBarController.ID_TV))
				mServerSocket.sendToChannel(fifo_message.remove(0), mRoom);
			else
				break;
		}
	}

	public void send(String message) {
		mServerSocket.sendToChannel(message, mRoom);
	}

	// /
	private JSONObject _seat_map = new JSONObject();
	private int _group_count = 1;

	public void setSeatMap(String seat_map, boolean sendUpdate) {

		JSONObject seat_map_json;

		try {

			seat_map_json = new JSONObject(seat_map);
			_group_count = seat_map_json.length();

			_seat_map = seat_map_json;

		} catch (JSONException e1) {
			e1.printStackTrace();
		}

		if (mServerSocket.countInChannel(mRoom) <= 0)
			return;

		if (!sendUpdate)
			return;

		// ///////////////////////////////////////////
		JSONObject json_object = new JSONObject();
		try {
			json_object.put("group_count", _group_count);
			json_object.put("seat_map", _seat_map);

		} catch (Exception e) {

			Log.i("ANE", "setSeatMap " + e.toString());

		}

		String message = json_object.toString();

		if (isFiFo) {
			fifo_message.add(message);
			sendRemaining();
		} else {
			send(message);
		}
	}

	public void sendAllUpdate() {
		if (mServerSocket.countInChannel(mRoom) <= 0)
			return;
		// ///////////////////////////////////////////
		JSONObject json_object = new JSONObject();
		try {
			json_object.put("group_count", _group_count);
			json_object.put("seat_map", _seat_map);

		} catch (Exception e) {

			Log.i("ANE", "setSeatMap " + e.toString());

		}

		String message = json_object.toString();

		if (isFiFo) {
			fifo_message.add(message);
			sendRemaining();
		} else {
			send(message);
		}
	}
}
